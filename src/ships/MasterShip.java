package ships;
import game.GameManager;
import strategy.*;

public class MasterShip extends Ship 
{

	// Mode data
	protected OperationalMode mode;
	
	
	public MasterShip()
	{
		this.name = "master,";
		// Default to defensive mode on start
		mode = new Defensive();
	}
	
	// Get master ship mode
	public OperationalMode getOperationalMode()
	{
		return this.mode;
	}
	
	// Set master ship mode
	public void setOperationalMode(OperationalMode mode)
	{
		this.mode = mode;
	}

	// Call resolve method based on current mode
	public void resolveCollisions(String[] col, GameManager gm)
	{
		this.mode.resolve(col, gm);
	}
	
	


	


	
}
