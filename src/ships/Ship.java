package ships;



public abstract class Ship 
{
	
	// Position data
	public int pos;
	// Ship name
	public String name;
	// Has it been updated
	public boolean updated = false;
	
	// Set position data
	public void setPos(int pos)
	{
		this.pos = pos;
	}
	
	// Set position coming from threads
	public void setPos(int pos, boolean changed)
	{
		this.pos = pos;
		updated = true;
	}
	// Print name
	public void PrintDetails()
	{
		System.out.println("I am a " + name);
	}
	
}
