package command;

import game.GameManager;

public interface Command {

		public void execute();
}
