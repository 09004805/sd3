package command;

import game.GameManager;
import java.util.*;


public class Commander {

	// Holds all move commands
	public ArrayList<MoveCommand> moveCommands = new ArrayList<MoveCommand>();
	
	// Adds a new move command to the commands list
	public void AddCommand(MoveCommand mc)
	{
		this.moveCommands.add(mc);
	}
	
	// Calls execute on all move commands
	public void executeCommands()
	{
		for (MoveCommand mc : moveCommands)
		{
			mc.execute();
		}
	}
	
	
	
}
