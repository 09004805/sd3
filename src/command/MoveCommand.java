package command;

import game.GameManager;
import ships.*;

public class MoveCommand implements Command {

	// Ship to move - string name or obj?
	public Ship ship;
	// Location to move to 
	public int newPos;

	
	public MoveCommand(Ship ship, int newPos)
	{
		this.ship = ship;
		this.newPos = newPos;		
	}
	
	

	
	// Carry out move command
	public void execute() {

		// Update ship position info
		this.ship.pos = this.newPos;		
	}

}
