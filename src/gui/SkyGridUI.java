package gui;

import game.GameManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.print.attribute.standard.Media;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.*;
import javax.*;

import observer.Observer;

import java.io.*;

import sun.audio.*;


public class SkyGridUI extends JFrame implements Observer {
	// Window
	private JPanel contentPane;
	
	// Images to be used
	ImageIcon bg = new ImageIcon("images/spacebg.jpg");	
	ImageIcon bh = new ImageIcon("images/blackhole.jpg");
	ImageIcon master = new ImageIcon("images/mastership.png");
	ImageIcon bstar = new ImageIcon("images/star.jpg");
	ImageIcon bshoot = new ImageIcon("images/shooter.jpg");
	ImageIcon bcruise = new ImageIcon("images/cruiser.jpg");
	
	
	// Label container
	ArrayList<JLabel> labels = new ArrayList<JLabel>();
	
	// Music properties
	String mp3File = "sounds/tlmusic.wav";
	Clip music;
	AudioInputStream ais;
	
	
	// Constructor for window
	public SkyGridUI(final GameManager gm) {
		// Start music
		try
		{
			music = AudioSystem.getClip();
			ais = AudioSystem.getAudioInputStream(new File(mp3File));
			music.open(ais);
			music.start();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
		
		// Ensure closes when pressing X
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Run full screen
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		// Split between label grid and buttons
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane);
		
		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);
		
		ButtonGroup btnGroup = new ButtonGroup();
		
		
		JButton btnMove = new JButton("Move");
		btnMove.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				// Ask GM to tell ships to move
				gm.BeginMoves();
				
			}});
		panel.add(btnMove);
		
		JRadioButton rdbtnOffensive = new JRadioButton("Offensive");
		rdbtnOffensive.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent arg0) {
				// Get GM to swap modes
				gm.ChangeMode("offensive");				
		}});
		btnGroup.add(rdbtnOffensive);
		panel.add(rdbtnOffensive);
		
		JRadioButton rdbtnDefensive = new JRadioButton("Defensive", true);
		rdbtnDefensive.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent arg0) {
				// Get GM to swap modes
				gm.ChangeMode("defensive");
			}});
		btnGroup.add(rdbtnDefensive);
		panel.add(rdbtnDefensive);
		
		
		JButton btnUndo = new JButton("Undo");
		btnUndo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// Ask GM to revert game state
				gm.UndoMove();
			}});
		panel.add(btnUndo);
		
		JPanel panel_1 = new JPanel();
		splitPane.setRightComponent(panel_1);
		panel_1.setLayout(new GridLayout(4, 4, 0, 0));
		
		
		// Create container label
		JLabel lbl00 = new JLabel(bh);
		// Add to labels container
		labels.add(lbl00);
		// Set layout to allow multiple labels to appear
		lbl00.setLayout(new BoxLayout(lbl00, BoxLayout.X_AXIS));
		// Add to panel
		panel_1.add(lbl00);
		
		// Container label
		JLabel lbl10 = new JLabel();
		// Add to labels container
		labels.add(lbl10);
		// Set layout to allow multiple labels
		lbl10.setLayout(new BoxLayout(lbl10, BoxLayout.X_AXIS));
		// Add to panel
		panel_1.add(lbl10);
		
		// Container label
		JLabel lbl20 = new JLabel();
		// Add to labels container
		labels.add(lbl20);
		// Set layout to allow multiple labels
		lbl20.setLayout(new BoxLayout(lbl20, BoxLayout.X_AXIS));
		// Add to panel
		panel_1.add(lbl20);
		
		// So on...
		JLabel lbl30 = new JLabel();
		labels.add(lbl30);
		lbl30.setLayout(new BoxLayout(lbl30, BoxLayout.X_AXIS));
		panel_1.add(lbl30);
		
		
		JLabel lbl01 = new JLabel();
		labels.add(lbl01);
		lbl01.setLayout(new BoxLayout(lbl01, BoxLayout.X_AXIS));
		panel_1.add(lbl01);
		
		JLabel lbl11 = new JLabel();
		labels.add(lbl11);
		lbl11.setLayout(new BoxLayout(lbl11, BoxLayout.X_AXIS));
		panel_1.add(lbl11);
		
		JLabel lbl21 = new JLabel();
		labels.add(lbl21);
		lbl21.setLayout(new BoxLayout(lbl21, BoxLayout.X_AXIS));
		panel_1.add(lbl21);
		
		JLabel lbl31 = new JLabel();
		labels.add(lbl31);
		lbl31.setLayout(new BoxLayout(lbl31, BoxLayout.X_AXIS));
		panel_1.add(lbl31);
		
		JLabel lbl02 = new JLabel();
		labels.add(lbl02);
		lbl02.setLayout(new BoxLayout(lbl02, BoxLayout.X_AXIS));
		panel_1.add(lbl02);
		
		JLabel lbl12 = new JLabel();
		labels.add(lbl12);
		lbl12.setLayout(new BoxLayout(lbl12, BoxLayout.X_AXIS));
		panel_1.add(lbl12);
		
		JLabel lbl22 = new JLabel();
		labels.add(lbl22);
		lbl22.setLayout(new BoxLayout(lbl22, BoxLayout.X_AXIS));
		panel_1.add(lbl22);
		
		JLabel lbl32 = new JLabel();
		labels.add(lbl32);
		lbl32.setLayout(new BoxLayout(lbl32, BoxLayout.X_AXIS));
		panel_1.add(lbl32);
		
		JLabel lbl03 = new JLabel();
		labels.add(lbl03);
		lbl03.setLayout(new BoxLayout(lbl03, BoxLayout.X_AXIS));
		panel_1.add(lbl03);
		
		JLabel lbl13 = new JLabel();
		labels.add(lbl13);
		lbl13.setLayout(new BoxLayout(lbl13, BoxLayout.X_AXIS));
		panel_1.add(lbl13);
		
		JLabel lbl23 = new JLabel();
		labels.add(lbl23);
		lbl23.setLayout(new BoxLayout(lbl23, BoxLayout.X_AXIS));
		panel_1.add(lbl23);
		
		JLabel lbl33 = new JLabel();
		labels.add(lbl33);
		lbl33.setLayout(new BoxLayout(lbl33, BoxLayout.X_AXIS));
		panel_1.add(lbl33);
		
		
	}

	
	// Observer update on game state change
	// Redraw grid with  new ship positions
	public void Update(String[] gameState) {
	
		// Clear labels first
		for (JLabel lbl : labels)
		{
			lbl.removeAll();
		}
		
		// Find empties in game state, add blank bg to labels
		for (int i = 0; i < gameState.length; i++)
		{
			if (gameState[i].equals("") || gameState[i].equals(" "))
			{
				// Add bg label to container at i
				labels.get(i).add(new JLabel(bg));				
			}
		}
	
		
		// Regex for master search
		String masterPtrn = "(.*master.*)";
		String starPtrn = "(.*star.*)";
		String shooterPtrn = "(.*shooter.*)";
		String cruiserPtrn = "(.*cruiser.*)";
		// Create pattern and matcher for using regex
		Pattern ptrn;
		Matcher matchmaker;
		
	
		// Search for enemies in game state
		for (String s : gameState)
		{
			// Check for master ship first
			ptrn = Pattern.compile(masterPtrn);
			matchmaker = ptrn.matcher(s);
			// If master is found
			if (matchmaker.find())
			{
				int masterPos = Arrays.asList(gameState).indexOf(s);
				//System.out.println("Found: "+ matchmaker.group(0)+ " at pos: "+ masterPos);
				labels.get(masterPos).add(new JLabel(master));
			}
			// Check for enemy ships; star first
			ptrn = Pattern.compile(starPtrn);
			matchmaker = ptrn.matcher(s);
			if (matchmaker.find())
			{
				int starPos = Arrays.asList(gameState).indexOf(s);
				//System.out.println("Found "+ matchmaker.group(0)+"at pos: "+ starPos);
				labels.get(starPos).add(new JLabel(bstar));
			}
			// Next check for shooters!
			ptrn = Pattern.compile(shooterPtrn);
			matchmaker = ptrn.matcher(s);
			if (matchmaker.find())
			{
				int shooterPos = Arrays.asList(gameState).indexOf(s);
				//System.out.println("Found "+matchmaker.group(0)+ "at pos "+ shooterPos);
				labels.get(shooterPos).add(new JLabel(bshoot));				
			}
			// Then check for cruisers!
			ptrn = Pattern.compile(cruiserPtrn);
			matchmaker = ptrn.matcher(s);
			if (matchmaker.find())
			{
				int cruiserPos = Arrays.asList(gameState).indexOf(s);
				//System.out.println("Found "+matchmaker.group(0)+"at pos "+ cruiserPos);
				labels.get(cruiserPos).add(new JLabel(bcruise));
			}
			
		}
		
		
		// Show changes
		contentPane.revalidate();
		contentPane.repaint();
	}

	
	// On end game, exit application
	public void CloseGUI()
	{
		// Stop music
		music.close();
		System.exit(0);
	}




	
	
}
