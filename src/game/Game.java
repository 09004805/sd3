package game;

import java.util.*;
import factory.*;
import gui.*;
import ships.*;

public class Game
{


	public static void main(String[] args)
	{
		
		
		// Instantiate classes	
		GameManager gm = new GameManager();
		SkyGridUI gui = new SkyGridUI(gm);
		
		
		// Add gui as listener to gm
		gm.addListener(gui);
		
			
		// Show gui
		gui.setVisible(true);
		
		

	}

}
