package game;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import observer.Observable;

import java.util.*;

import command.Commander;
import command.MoveCommand;
import ships.MasterShip;
import ships.Ship;
import strategy.*;
import threads.ShipLocator;
import factory.ShipFactory;
import gui.*;


public class GameManager implements Observable  {

	
	// Instantiate classes
	ShipFactory factory = new ShipFactory();
	Commander cmdr = new Commander();
	
	// List of created ships in game
	public ArrayList<Ship> ships = new ArrayList<Ship>();
	public ArrayList<ArrayList<Ship>> shipStates = new ArrayList<ArrayList<Ship>>();
	// Tracks game state; ship grid pos = gameState index
	public String[] gameState = new String[16];
	
	// Listeners
	private SkyGridUI listenerUI; 

	// To hold all available move arrays below
	public ArrayList<Integer[]> validMoves = new ArrayList<Integer[]>();
	// Available moves based on grid pos
	Integer[] moves0 = {1, 4, 5};
	Integer[] moves1 = {2, 4, 5, 6};
	Integer[] moves2 = {1, 3, 5, 6, 7};
	Integer[] moves3 = {2, 6, 7};
	Integer[] moves4 = {1, 5, 8, 9};
	Integer[] moves5 = {1, 2, 4, 6, 8, 9, 10};
	Integer[] moves6 = {1, 2, 3, 5, 7, 9, 10, 11};
	Integer[] moves7 = {2, 3, 6, 10, 11};
	Integer[] moves8 = {4, 5, 9, 12, 13};
	Integer[] moves9 = {4, 5, 6, 8, 10, 12, 13, 14};
	Integer[] moves10 = {5, 6, 7, 9, 11, 13, 14, 15};
	Integer[] moves11 = {6, 7, 10, 14, 15};
	Integer[] moves12 = { 8, 9, 13};
	Integer[] moves13 = {8, 9, 10, 12, 14};
	Integer[] moves14 = {9, 10, 11, 13, 15};
	Integer[] moves15 = {10, 11, 14};

	// Tracks number of Undo actions
	int undoCount = 0;
	
	// Setup Master Ship
	MasterShip master = factory.SetupMaster(this);
	
	// Sets up game state array & valid moves list
	// Finds a random start location for master ship
	public void SetupGame(Ship master)
	{
		// Add move arrays to valid moves list in order
		validMoves.add(moves0);
		validMoves.add(moves1);
		validMoves.add(moves2);
		validMoves.add(moves3);
		validMoves.add(moves4);
		validMoves.add(moves5);
		validMoves.add(moves6);
		validMoves.add(moves7);
		validMoves.add(moves8);
		validMoves.add(moves9);
		validMoves.add(moves10);
		validMoves.add(moves11);
		validMoves.add(moves12);
		validMoves.add(moves13);
		validMoves.add(moves14);
		validMoves.add(moves15);
	
		ResetGameState();
		// Find random number between 1-15
		int pos = ThreadLocalRandom.current().nextInt(1, 16);
		// Update game state with new position
		//gameState[pos] = master.name;	
		// Update master ship position
		master.setPos(pos);
		// Update game state
		UpdateGameState();
		
		// Notify listeners 
		this.notifyListeners();		
	}

	// Resets game state before a move takes place
	private void ResetGameState()
	{
		// Black hole always present top left
		gameState[0] = "blackhole,";
		// Set each other value to empty for now
		for (int i = 1; i < gameState.length; i++)
			gameState[i] = "";
	}
	
	// Serialises game state by writing to .txt file
	public void SaveGame()
	{
		try
		{
			// Create file
			FileWriter fw = new FileWriter("gameState.txt", true);
			// For every string in game state
			for (String s : gameState)
			{
				// Write it to file, followed by comma
				fw.write(s + ";");
			}
			// Add a new line
			fw.write("\n");
			// Show data, close file writer after
			fw.flush();
			fw.close();
		}
		// Just in case
		catch (IOException e)
		{
			// Show error
			System.out.println(e);
		}	
	}
	
	// Reverts to previous game state
	public void UndoMove()
	{		
		//Increase undo action count with each click
		undoCount++;
			
		// Open up the game state file
		try (BufferedReader br = new BufferedReader(new FileReader("gameState.txt")))
		{
			// Number of lines in file
			int lines = 0;		
			// Read all lines
			while ((br.readLine()) != null)
			{
				// Increase line count with each line
				lines++;
			}
						
			String str = "";
			String[] newGameState;
			
			// Find the required game state so long as undo count not higher than lines
			if (lines > undoCount)
			{
				// Read in the designated line
				str = Files.readAllLines(Paths.get("gameState.txt")).get(lines - undoCount);

				// Now split line on semi colons - this equals new game state
				newGameState = str.split(";", -1);
				
				// Overwrite game state with new one (new state has extra element at end from split)
				for ( int i = 0; i < newGameState.length - 1; i++)
				{
					// Copy over new state info
					gameState[i] = newGameState[i];
				}
				
				// Update gui now that game state has changed
				this.notifyListeners();
				
				ArrayList<ShipLocator> threads = new ArrayList<ShipLocator>();
				// Update ship positions in order to move correctly from here
				for (int i = 0; i < gameState.length; i++)
				{
					// Position of current string in game state
					int sPos = i;
					// Current string in game state at i
					String curStr = gameState[i];
					// Initialise thread
					ShipLocator seeker = new ShipLocator(sPos, curStr, ships);
					// Call Run on Start
					seeker.start();
					// Add to list of active threads
					threads.add(seeker);
					
					
					// Find master
					if (gameState[i].contains("master"))
						// Set it's position to that of index in list
						master.setPos(sPos);
				}
				
				// Close all threads
				for (ShipLocator sl : threads)
				{
					try
					{
						sl.join();
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				
				// Extra ships held here
				ArrayList<Ship> deadShips = new ArrayList<Ship>();
				// Remove extra ships not updated
				for (Ship s : ships)
				{
					// But not the master ship
					if (s.updated == false && s.name != "master,")
						deadShips.add(s);
					
					// Now reset flag to false
					s.updated = false;
					// And remove extras from list
					ships.removeAll(deadShips);
				}
					
					/*
					// Find battle star
					else if (s.contains("star"))
					{
						// Search through ships list for battle star
						for (Ship ship : ships)
						{
							// If the name is star, and current position isn't equal to old position
							if (ship.name.equals("star,") && ship.pos != sPos)
							{
								// Set to old position
								ship.setPos(sPos);
							}
						}
					}
					else if(s.contains("shooter"))
					{
						// Search through ships list for battle shooter
						for (Ship ship : ships)
						{
							// If the name is shooter, and current pos isn't equal to old pos
							if (ship.name.equals("shooter,") && ship.pos != sPos)
							{
								// Set to old pos
								ship.setPos(sPos);
							}
						}
					}
					else if (s.contains("cruiser"))
					{
						// Search through ships list for battle cruiser
						for (Ship ship : ships)
						{
							// If the name is cruisers, and current pos != old pos
							if (ship.name.equals("cruiser,") && ship.pos != sPos)
							{
								// Set to old pos
								ship.setPos(sPos);
							}
						}
					}
						*/
				
			}
			else
			{
				System.out.println("Can't undo that much!");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
			
	// Prompts all ships to make move commands,
	// Chance of enemy spawning
	public void BeginMoves()
	{
		// Reset undoCount
		undoCount = 0;
		// Save game state for future undo actions
		SaveGame();
		
		// Reset game state
		ResetGameState();
		
		// For every ship in play
		for (Ship s : ships)
		{
			// Make a move command and send to commander
			// First find out where to move to
			int newMove = RandomMove(s.pos);
			// Create a new move command
			MoveCommand mc = new MoveCommand(s, newMove);
			// Send to commander
			cmdr.AddCommand(mc);		
		}		
		// Carry out commands to update ships
		cmdr.executeCommands();
		
		// Check for potential enemy spawning, if true
		if (EnemySpawn())
		{
			// Add new enemy
			Ship enemy = factory.AddRandomEnemy();
			// Set start position
			enemy.pos = 0;
			// Add to ships list
			ships.add(enemy);
			// Declare!
			System.out.println(enemy.name + " entered the game!");
		}
		
		
		// Update game state to reflect new ship positions
		UpdateGameState();
		
		//SaveGame();
		// Notify listeners
		this.notifyListeners();
		
		// Check for collisions with enemy ships
		DetectCollisions();
	}
		
	// Checks if master ship shares a cell with enemy ships
	public void DetectCollisions()
	{
		// Pattern used to check for master
		String masterPtrn = "(.*master,.*)";
		Pattern ptrn = Pattern.compile(masterPtrn);
		Matcher matchmaker;
		// Check strings in game state for master ship entry
		for (String s : gameState)
		{
			// Search each string against master pattern above
			matchmaker = ptrn.matcher(s);
			// If a match is found
			if (matchmaker.find())
			{
				// Split the string containing master at commas
				String[] collidingShips = s.split(",");
				// If length is bigger than 1, enemies are present
				if (collidingShips.length > 1)
				{
					// Resolve collisions
					master.resolveCollisions(collidingShips, this);
				}		
			}
		}	
	}

	// Removes destroyed enemies 
	public void RemoveShips()
	{
		// List of ships to remove
		ArrayList<Ship> toDestroy = new ArrayList<Ship>();
		
		// Find ships to destroy
		for (Ship s : ships)
		{
			// Check each ships position against master pos
			// Ensure master itself isn't removed by checking against name
			if (s.pos == master.pos && s.name != "master,")
			{
				System.out.println("Removing " + s.name);
				// Add to list
				toDestroy.add(s);
			}
		}
		// Remove destroyed ships from ship list
		ships.removeAll(toDestroy);
		
		// Secondly, remove from game state at master pos by overwriting
		gameState[master.pos] = "master,";
	}
	
	// Stops the game by closing gui
	public void EndGame()
	{
		this.listenerUI.CloseGUI();
	}
	
	// Decides whether enemy should spawn based on 1/3 chance
	public boolean EnemySpawn()
	{
		// Find random value between 0, 1, 2
		Random rng = new Random();
		int value = rng.nextInt(3);
		// If value is 2, add enemy
		switch (value)
		{
			case 0:
				break;
			case 1:
				break;
			case 2:
				return true;
			
		}
		return false;
	}
	
	// Returns random move based on current cell
	public int RandomMove(int pos)
	{
		// Find potential moves stored in validMoves[][]
		Integer[] potentialMoves = validMoves.get(pos);
		// Of these, pick one at random
		int move;
		Random rng = new Random();
		// Array length value non inclusive
		move = rng.nextInt(potentialMoves.length);
		
		return potentialMoves[move];
	}
	
	// Carries out move order effects by altering game state 
	public void UpdateGameState()
	{
		// Reset first
		//this.ResetGameState();
		// Write ship name into new pos in array
		for (Ship s : ships)
		{
			// ship position = index of game state array
			int index = s.pos;
			// append name to list 
			gameState[index] += s.name;
		}	
	}

	
	// Swaps master ship mode on rad btn press
	public void ChangeMode(String mode)
	{
		// Check which mode to change to
		if (mode == "offensive")
		{
			// Swap to offensive mode
			master.setOperationalMode(new Offensive());
		}
		else if (mode == "defensive")
		{
			// Swap to defensive mode
			master.setOperationalMode(new Defensive());
		}
	}

	// Add gui listener
	public void addListener(SkyGridUI grid) {
		this.listenerUI = grid;	
	}
	
	
	// Notify listeners 
	public void notifyListeners() {
		// Game State has changed - UI to redraw
		if (listenerUI == null)
		{
			return;
		}
		
		for (String s : gameState)
			System.out.print(s+" ");
		System.out.println("");
		 
		System.out.println(ships.size()+ " ships in play");
		this.listenerUI.Update(gameState);
	}







	
}
