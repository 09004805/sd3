package strategy;

import game.GameManager;

public interface OperationalMode {
	public void resolve(String[] col, GameManager gm);
}
