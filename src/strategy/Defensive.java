package strategy;

import game.GameManager;

public class Defensive implements OperationalMode {

	// Offensive mode allows player to kill n ships
	// Defensive mode allows player to survive n+1 ships 
	public void resolve(String[] col, GameManager gm) {
		
		// Find number of present enemy ships
		// -1 because master ship is in col
		int enemies = col.length - 1;
		
		// If no of enemies is less than n+1, player survives encounter
		if (enemies <= 2)
		{
			System.out.println("Survived attack by " + enemies + " enemy ships");
		}
		// If no of enemies exceeds n+1, player dies
		if (enemies > 2)
		{
			System.out.println(enemies + " enemy ships attacked - player destroyed.");
			// End the game
			gm.EndGame();
		}

	}

}
