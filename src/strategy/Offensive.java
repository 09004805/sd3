package strategy;

import game.GameManager;

public class Offensive implements OperationalMode {

	// Offensive mode allows player to kill n ships
	// Defensive mode allows player to survive n+1 ships 
	public void resolve(String[] col, GameManager gm) {
		
		// Create list of enemy ships, without master included in col
		String[] enemyShips = new String[col.length - 1];
		// Removes master from start of col
		for (int i = 1; i < col.length - 1; i++)
		{
			enemyShips[i-1] = col[i];
		}
		
		// If no of enemies is equal to/less than n, all enemies are destroyed
		if (enemyShips.length <= 1)
		{
			// Send enemies to GM to remove from game state
			gm.RemoveShips();
			
			System.out.println(enemyShips.length + " enemy ships destroyed.");
		}
		// If no of enemies is more than n, player is destroyed
		if (enemyShips.length > 1)
		{
			System.out.println("Unable to destroy " + enemyShips.length + " enemy ships - player destroyed."); 
			gm.EndGame();
		}
	}

}
