package threads;

import java.util.*;
import ships.*;

public class ShipLocator extends Thread {

	// Reads strings in game state and compares with ship names to find ships
	// On finding matches, updates their position
	
	// Updated ship position in grid
	int pos;
	// String received from gameState[] 
	String gameStateString;
	// Ships to find matches against
	ArrayList<Ship> shipsList = new ArrayList<Ship>();
	// Ship names to check against shipsList
	String[] shipNames = {"star,","shooter,","cruiser,"};
	
	// Constructor to set properties
	public ShipLocator(int newPos, String gameStateSingle, ArrayList<Ship> shipList)
	{
		this.pos = newPos;
		this.gameStateString = gameStateSingle;
		this.shipsList = shipList;
	}
	
	
	// Search given string for matches in ship names
	@Override
	public void run()
	{
		System.out.println("Thread "+ pos +" running");
		for (int i = 0; i < shipNames.length - 1; i++)
		{
			// Check if master is present
			if (gameStateString.contains(shipNames[i]))
			{
				// Search through ships list for battle star
				for (Ship s : shipsList)
				{
					// If the name is matched, with an out dated position
					if (s.name.equals(shipNames[i]) && s.pos != this.pos)
					{
						// Update position
						System.out.println("Updating pos of " + s.name);
						s.setPos(this.pos, true);
					}
				}
			}
		}
		return;
	}
}
