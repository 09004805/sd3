package observer;

public interface Observer {

		public void Update(String[] gameState);
}
