package observer;
import ships.Ship;
import gui.*;

public interface Observable {
	public void addListener(SkyGridUI grid);
	public void notifyListeners();
}
