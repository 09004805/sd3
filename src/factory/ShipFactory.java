package factory;
import ships.*;
import game.GameManager;

import java.util.Random;

public class ShipFactory 
{

	// Setup Master Ship
	public MasterShip SetupMaster(GameManager gm)
	{
		// Create master ship obj
		MasterShip master = new MasterShip();
		// Add to GM list of created ships
		gm.ships.add(master);
		// Call setup method passing master ship
		gm.SetupGame(master);
		return master;
	}
	
	
	// Add Random Enemy - randomly selects enemy type to create
	public Ship AddRandomEnemy()
	{
		// Setup enemy ship object
		Ship enemy = null;
		// Select random value
		Random rand = new Random();
		int choice = rand.nextInt(3);
		// Work based on random choice
		switch (choice)
		{
			case 0:
				enemy = new BattleCruiser();
				break;
				
			case 1:
				enemy = new BattleStar();
				break;
				
			case 2:
				enemy = new BattleShooter();
				break;
		}
		
		return enemy;	
	}
	
	
	
		
		
		
	
}
